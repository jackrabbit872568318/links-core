module gitee.com/jackrabbit872568318/links-core

go 1.18

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/gofrs/uuid v4.3.0+incompatible
	github.com/gorilla/websocket v1.5.0
	github.com/klauspost/compress v1.15.11
	github.com/shirou/gopsutil/v3 v3.22.9
	github.com/songgao/water v0.0.0-20200317203138-2b4b6d7c09d8
	github.com/xtaci/kcp-go v5.4.20+incompatible
	golang.zx2c4.com/wireguard v0.0.0-20220920152132-bb719d3a6e2c
)

require (
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/klauspost/cpuid/v2 v2.1.1 // indirect
	github.com/klauspost/reedsolomon v1.11.1 // indirect
	github.com/lufia/plan9stats v0.0.0-20211012122336-39d0f177ccd0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/power-devops/perfstat v0.0.0-20210106213030-5aafc221ea8c // indirect
	github.com/templexxx/cpufeat v0.0.0-20180724012125-cef66df7f161 // indirect
	github.com/templexxx/xor v0.0.0-20191217153810-f85b25db303b // indirect
	github.com/tjfoc/gmsm v1.4.1 // indirect
	github.com/tklauser/go-sysconf v0.3.10 // indirect
	github.com/tklauser/numcpus v0.4.0 // indirect
	github.com/xtaci/lossyconn v0.0.0-20200209145036-adba10fffc37 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	golang.org/x/crypto v0.0.0-20221005025214-4161e89ecf1b // indirect
	golang.org/x/net v0.0.0-20221004154528-8021a29435af // indirect
	golang.org/x/sys v0.0.0-20220728004956-3c1f35247d10 // indirect
	golang.zx2c4.com/wintun v0.0.0-20211104114900-415007cec224 // indirect
)
