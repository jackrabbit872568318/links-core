package utils

import (
	"gitee.com/jackrabbit872568318/links-core/logger"
	"os"
	"os/signal"
	"syscall"
)

//
// StopWatcher
// @Description:
// @param after
//
func StopWatcher(after func()) {
	go func() {
		osc := make(chan os.Signal, 1)
		signal.Notify(osc, syscall.SIGTERM, syscall.SIGINT)
		s := <-osc
		logger.Info()
		logger.Info("receive stop signal : ", s)
		after()
	}()
}
