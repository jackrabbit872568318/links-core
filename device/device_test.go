package device

import (
	"fmt"
	"testing"
	"time"
)

var conf = Config{
	CIDR: "10.200.187.2/24",
	MTU:  1400,
	DNS:  "",
	Name: "",
}

//
// TestNewAndSetup
//  @Description:
//  @param t
//
func TestNewAndSetup(t *testing.T) {
	device, err := New(conf)
	if err != nil {
		t.Fatal(err)
		return
	}
	fmt.Println(device.Name(), "created")
	err = device.Setup()
	if err != nil {
		t.Fatal(err)
		return
	}
	time.Sleep(time.Second * 10)
	fmt.Println("done!")
}

//
// TestTunDevice_UpDown
//  @Description:
//  @param t
//
func TestTunDevice_UpAndDown(t *testing.T) {
	device, err := New(conf)
	if err != nil {
		t.Fatal(err)
		return
	}
	fmt.Println(device.Name(), "created")
	err = device.Setup()
	if err != nil {
		t.Fatal(err)
		return
	}
	err = device.Up()
	if err != nil {
		t.Fatal(err)
		return
	}
	fmt.Println("device up")
	time.Sleep(time.Second * 5)
	err = device.Down()
	if err != nil {
		t.Fatal(err)
		return
	}
	fmt.Println("device down")
	time.Sleep(time.Second * 5)
	fmt.Println("done!")
}

//
// TestTunDevice_OverwriteCIDR
//  @Description:
//  @param t
//
func TestTunDevice_OverwriteCIDR(t *testing.T) {
	device, err := New(conf)
	if err != nil {
		t.Fatal(err)
		return
	}
	fmt.Println(device.Name(), "created")
	err = device.Setup()
	if err != nil {
		t.Fatal(err)
		return
	}
	fmt.Println("overwrite after 10s")
	time.Sleep(time.Second * 10)
	err = device.OverwriteCIDR("10.200.187.3/24")
	fmt.Println("overwrite -> 10.200.187.3/24")
	if err != nil {
		t.Fatal(err)
		return
	}
	time.Sleep(time.Second * 10)
	fmt.Println("done!")
}
