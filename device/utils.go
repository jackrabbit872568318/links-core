package device

import (
	"fmt"
	"math/rand"
	"time"
)

//
// ipv4MaskString
// @Description:
// @param m
// @return string
//W
func ipv4MaskString(m []byte) string {
	if len(m) != 4 {
		panic("ipv4Mask: len must be 4 bytes")
	}
	return fmt.Sprintf("%d.%d.%d.%d", m[0], m[1], m[2], m[3])
}

//
// randString
// @Description:
// @param l
// @return string
//
func randString(l int) string {
	str := "0123456789abcdefghigklmnopqrstuvwxyz"
	strList := []byte(str)

	var result []byte
	i := 0

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i < l {
		s := strList[r.Intn(len(strList))]
		result = append(result, s)
		i = i + 1
	}
	return string(result)
}
