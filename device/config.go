package device

type Config struct {
	Name string `json:"name"` //设备名称
	CIDR string `json:"cidr"` //cidr地址
	MTU  int    `json:"mtu"`  //最大传输单元,默认1400
	DNS  string `json:"dns"`  //dns地址,仅在windows生效
}
