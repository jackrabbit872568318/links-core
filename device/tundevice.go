package device

import (
	"errors"
	"gitee.com/jackrabbit872568318/links-core/logger"
	"gitee.com/jackrabbit872568318/links-core/utils"
)

//
// Device
// @Description:
//
type Device interface {
	Name() string                           //return device name if device exists
	Create(config Config) error             //create device
	Close() error                           //close device
	Setup() error                           //setup device
	OverwriteCIDR(cidr string) error        //overwrite cidr
	Up() error                              //set device up
	Down() error                            //set device down
	Read(packet []byte) (n int, err error)  //read
	Write(packet []byte) (n int, err error) //write
}

//
// New
// @Description:
// @param cfg
// @return device
// @return err
//
func New(cfg Config) (device Device, err error) {
	d := TunDevice{}
	if cfg.MTU > 1500 || cfg.MTU <= 0 {
		logger.Info("invalid mtu value reset ", cfg.MTU, " -> 1400")
		cfg.MTU = 1400
	}
	if cfg.CIDR == "" {
		return nil, errors.New("cidr not set")
	}
	if cfg.Name == "" {
		cfg.Name = "tunn-" + randString(8)
		logger.Info("set device name : ", cfg.Name)
	}
	err = d.Create(cfg)

	utils.StopWatcher(func() {
		_ = d.Close()
		logger.Info("device closed : ", cfg.Name)
	})
	return &d, err
}
