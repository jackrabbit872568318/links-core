package transmitter

import (
	"fmt"
	"testing"
)

func TestBytes2LenConvertor_Convert(t *testing.T) {
	convertor := newBytes2LenConvertor()
	b1 := byte(5)
	b2 := byte(220)
	fmt.Println(convertor.Convert(b1, b2))
}

func BenchmarkBytes2LenConvertor_Convert(b *testing.B) {
	convertor := newBytes2LenConvertor()
	b1 := byte(5)
	b2 := byte(220)
	for i := 0; i < b.N; i++ {
		convertor.Convert(b1, b2)
	}
}

func TestLen2BytesConvertor_Convert(t *testing.T) {
	convertor := newLen2BytesConvertor()
	fmt.Println(convertor.Convert(1500))
}

func BenchmarkLen2BytesConvertor_Convert(b *testing.B) {
	convertor := newLen2BytesConvertor()
	for i := 0; i < b.N; i++ {
		convertor.Convert(uint16(1500))
	}
}
