package transmitter

import (
	"io"
	"sync"
)

var (
	DefaultSymbol = []byte{255, 254}
	Empty         = make([]byte, 0)
	Blank         = byte(0)
)

type packer struct {
	l2b    *len2BytesConvertor
	b2l    *bytes2LenConvertor
	header []byte
}

func newPacker() *packer {
	return &packer{
		l2b:    newLen2BytesConvertor(),
		b2l:    newBytes2LenConvertor(),
		header: make([]byte, 6),
	}
}

// Pack 封装byte数组为指定格式
func (p *packer) Pack(pl []byte) []byte {
	if l := len(pl); l == 0 {
		return append(DefaultSymbol, 0, 0, 0, 0)
	} else {
		length := p.l2b.Convert(uint16(l))
		return append([]byte{DefaultSymbol[0], DefaultSymbol[1], 0, length[0], length[1], 0}, pl...)
	}
}

// ReadOnePacket 尝试从io中读取一个包
func (p *packer) ReadOnePacket(rwc io.ReadWriteCloser) (payload []byte, n int, err error) {
	for {
		n, err = rwc.Read(p.header)
		if err != nil {
			return Empty, 0, err
		}
		if n != 6 {
			continue
		}
		if p.header[0] != DefaultSymbol[0] ||
			p.header[1] != DefaultSymbol[1] ||
			p.header[2] != Blank ||
			p.header[5] != Blank {
			continue
		}
		break
	}

	length := p.b2l.Convert(p.header[3], p.header[4])

	payload = make([]byte, length)
	n, err = rwc.Read(payload)
	if err != nil {
		return nil, 0, err
	}

	return payload, n, nil
}

type concurrencyPacker struct {
	sync.RWMutex
	l2b    *len2BytesConvertor
	b2l    *bytes2LenConvertor
	header []byte
}

func newConcurrencyPacker() *concurrencyPacker {
	return &concurrencyPacker{
		l2b:    newLen2BytesConvertor(),
		b2l:    newBytes2LenConvertor(),
		header: make([]byte, 6),
	}
}

// Pack 封装byte数组为指定格式
func (p *concurrencyPacker) Pack(pl []byte) []byte {
	if l := len(pl); l == 0 {
		return append(DefaultSymbol, 0, 0, 0, 0)
	} else {
		p.Lock()
		defer p.Unlock()
		length := p.l2b.Convert(uint16(l))
		return append([]byte{DefaultSymbol[0], DefaultSymbol[1], 0, length[0], length[1], 0}, pl...)
	}
}

// ReadOnePacket 尝试从io中读取一个包
func (p *concurrencyPacker) ReadOnePacket(rwc io.ReadWriteCloser) (payload []byte, n int, err error) {
	p.Lock()
	defer p.Unlock()
	for {
		n, err = rwc.Read(p.header)
		if err != nil {
			return Empty, 0, err
		}
		if n != 6 {
			continue
		}
		if p.header[0] != DefaultSymbol[0] ||
			p.header[1] != DefaultSymbol[1] ||
			p.header[2] != Blank ||
			p.header[5] != Blank {
			continue
		}
		break
	}

	length := p.b2l.Convert(p.header[3], p.header[4])

	payload = make([]byte, length)
	n, err = rwc.Read(payload)
	if err != nil {
		return nil, 0, err
	}

	return payload, n, nil
}
