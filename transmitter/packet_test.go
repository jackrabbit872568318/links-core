package transmitter

import (
	"fmt"
	"math/rand"
	"testing"
)

var bs []byte

func init() {
	for i := 0; i < 1394; i++ {
		bs = append(bs, byte(rand.Intn(254)))
	}
}

func TestPacker_Pack(t *testing.T) {
	p := newPacker()
	bytes := []byte("hello")
	fmt.Println("before --> ", bytes)
	packed := p.Pack(bytes)
	fmt.Println("after  --> ", packed)
}

func BenchmarkPacker_Pack(b *testing.B) {
	p := newPacker()
	for i := 0; i < b.N; i++ {
		p.Pack(bs)
	}
}

func TestConcurrencyPacker_Pack(t *testing.T) {
	p := newConcurrencyPacker()
	fmt.Println("before --> ", bs)
	packed := p.Pack(bs)
	fmt.Println("after  --> ", packed)
}

func BenchmarkConcurrencyPacker_Pack(b *testing.B) {
	p := newConcurrencyPacker()
	for i := 0; i < b.N; i++ {
		p.Pack(bs)
	}
}
