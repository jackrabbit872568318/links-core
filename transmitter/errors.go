package transmitter

import "errors"

var (
	ErrBadPacket = errors.New("bad packet")
	ErrBadHeader = errors.New("bad header")
)
