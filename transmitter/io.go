package transmitter

import (
	"net"
	"time"
)

type IO struct {
	rwc    net.Conn
	packer *packer
}

func WrapConn(conn net.Conn) *IO {
	return &IO{
		rwc:    conn,
		packer: newPacker(),
	}
}

func (io *IO) ReadOnePacket() (p []byte, n int, err error) {
	return io.packer.ReadOnePacket(io.rwc)
}

func (io *IO) Read(p []byte) (n int, err error) {
	return io.rwc.Read(p)
}

func (io *IO) Write(p []byte) (n int, err error) {
	return io.rwc.Write(io.packer.Pack(p))
}

func (io *IO) Close() error {
	return io.rwc.Close()
}

func (io *IO) LocalAddr() net.Addr {
	return io.rwc.LocalAddr()
}

func (io *IO) RemoteAddr() net.Addr {
	return io.rwc.RemoteAddr()
}

func (io *IO) SetDeadline(t time.Time) error {
	return io.SetDeadline(t)
}

func (io *IO) SetReadDeadline(t time.Time) error {
	return io.SetReadDeadline(t)
}

func (io *IO) SetWriteDeadline(t time.Time) error {
	return io.SetWriteDeadline(t)
}

type ConcurrencyIO struct {
	rwc    net.Conn
	packer *concurrencyPacker
}

func WrapConcurrentConn(conn net.Conn) *ConcurrencyIO {
	return &ConcurrencyIO{
		rwc:    conn,
		packer: newConcurrencyPacker(),
	}
}

func (io *ConcurrencyIO) ReadOnePacket() (p []byte, n int, err error) {
	return io.packer.ReadOnePacket(io.rwc)
}

func (io *ConcurrencyIO) Read(p []byte) (n int, err error) {
	return io.rwc.Read(p)
}

func (io *ConcurrencyIO) Write(p []byte) (n int, err error) {
	return io.rwc.Write(io.packer.Pack(p))
}

func (io *ConcurrencyIO) Close() error {
	return io.rwc.Close()
}

func (io *ConcurrencyIO) LocalAddr() net.Addr {
	return io.rwc.LocalAddr()
}

func (io *ConcurrencyIO) RemoteAddr() net.Addr {
	return io.rwc.RemoteAddr()
}

func (io *ConcurrencyIO) SetDeadline(t time.Time) error {
	return io.SetDeadline(t)
}

func (io *ConcurrencyIO) SetReadDeadline(t time.Time) error {
	return io.SetReadDeadline(t)
}

func (io *ConcurrencyIO) SetWriteDeadline(t time.Time) error {
	return io.SetWriteDeadline(t)
}
