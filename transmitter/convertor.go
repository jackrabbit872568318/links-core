package transmitter

import (
	"bytes"
	"encoding/binary"
)

type len2BytesConvertor struct {
	buf *bytes.Buffer
}

func newLen2BytesConvertor() *len2BytesConvertor {
	return &len2BytesConvertor{
		buf: bytes.NewBuffer([]byte{}),
	}
}

func (c *len2BytesConvertor) Convert(u uint16) []byte {
	defer c.buf.Reset()
	_ = binary.Write(c.buf, binary.BigEndian, &u)
	return c.buf.Bytes()
}

type bytes2LenConvertor struct {
	buf *bytes.Buffer
}

func newBytes2LenConvertor() *bytes2LenConvertor {
	return &bytes2LenConvertor{
		buf: bytes.NewBuffer([]byte{}),
	}
}

func (c *bytes2LenConvertor) Convert(b1, b2 byte) (l uint16) {
	defer c.buf.Reset()
	c.buf.Write([]byte{b1, b2})
	_ = binary.Read(c.buf, binary.BigEndian, &l)
	return
}
