package network

import "errors"

var (
	ErrMaskSizeOutOfLimit = errors.New("net mask size out of limit")
)
