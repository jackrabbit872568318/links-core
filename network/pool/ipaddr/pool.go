package ipaddr

import (
	"encoding/binary"
	"errors"
	"net"
	"strings"
	"sync"
)

var (
	ErrAddressPoolNotAvailable = errors.New("address pool not Available")
	ErrAddressOutOfRange       = errors.New("address out of range")
	ErrAddressInUse            = errors.New("address in use")
)

type PoolV4 struct {
	mutex      sync.Mutex
	cidr       string
	net        *net.IPNet
	start, end uint32
	mask       int
	used       map[uint32]struct{}
}

// NewPool constructor
func NewPool(cidr string) (p *PoolV4, err error) {
	ip, ipNet, err := net.ParseCIDR(cidr)
	if err != nil {
		return
	}
	maskOnes, _ := ipNet.Mask.Size()
	wildcard := uint32byte(0xFFFFFFFF >> maskOnes)
	p = &PoolV4{
		cidr:  cidr,
		net:   ipNet,
		mask:  maskOnes,
		start: binary.BigEndian.Uint32(ipNet.IP) | 1<<0,
		end: binary.BigEndian.Uint32(
			uint32byte(
				binary.BigEndian.Uint32(ip.To4())|binary.BigEndian.Uint32(wildcard),
			),
		) &^ 1 << 0,
		used: map[uint32]struct{}{},
	}
	return
}

// GetRandom return an address if available
func (p *PoolV4) GetRandom() (string, error) {
	return p.random()
}

// Get ip address from pool
func (p *PoolV4) Get(ip string) (addr string, err error) {
	parseIP := net.ParseIP(ip)
	if !p.net.Contains(parseIP) {
		err = ErrAddressOutOfRange
		return
	}

	p.mutex.Lock()
	defer p.mutex.Unlock()

	address := ipAddress(parseIP.To4())
	if _, ok := p.used[address.uint32()]; ok {
		err = ErrAddressInUse
		return
	}
	p.used[address.uint32()] = struct{}{}
	return address.CIDR(p.mask), nil
}

// Put ip back to current pool
func (p *PoolV4) Put(ip string) error {
	ip = strings.Split(ip, "/")[0]
	parseIP := net.ParseIP(ip)
	if !p.net.Contains(parseIP) {
		return ErrAddressOutOfRange
	}

	p.mutex.Lock()
	defer p.mutex.Unlock()

	address := ipAddress(parseIP.To4())
	idx := address.uint32()
	if _, ok := p.used[idx]; ok {
		delete(p.used, idx)
	}

	return nil
}

// Size pool size
func (p *PoolV4) Size() uint32 {
	return p.end - p.start + 1
}

// Used number of address
func (p *PoolV4) Used() int {
	return len(p.used)
}

// Available shows pool is available
func (p *PoolV4) Available() bool {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	return p.end-p.start >= uint32(len(p.used))
}

func (p *PoolV4) random() (addr string, err error) {
	if !p.Available() {
		err = ErrAddressPoolNotAvailable
		return
	}
	p.mutex.Lock()
	defer p.mutex.Unlock()

	var val uint32

	for count := 0; count < 1; {
		val = randInt32(p.start, p.end, getRand())
		if _, ok := p.used[val]; !ok {
			p.used[val] = struct{}{}
			count++
		}
	}
	address := ipAddress(uint32byte(val))
	return address.CIDR(p.mask), nil
}
