package ipaddr

import (
	"fmt"
	"testing"
	"time"
)

func TestNewPool(t *testing.T) {
	pool, err := NewPool("192.168.3.0/24")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("%+v\n", pool)
	start := ipAddress(uint32byte(pool.start))
	end := ipAddress(uint32byte(pool.end))
	fmt.Printf("start : %v\n", start)
	fmt.Printf("end   : %v\n", end)
}

func TestPoolV4_Random(t *testing.T) {
	pool, err := NewPool("172.16.8.0/28")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("%+v\n", pool)
	start := ipAddress(uint32byte(pool.start))
	end := ipAddress(uint32byte(pool.end))
	fmt.Printf("start : %v\n", start)
	fmt.Printf("end   : %v\n", end)
	fmt.Println("------------------------------------------------------")

	for i := 0; i < 255*255*254; i++ {
		start := time.Now().Nanosecond()
		random, err := pool.GetRandom()
		span := time.Now().Nanosecond() - start
		if err != nil {
			fmt.Println(err)
			return
		}
		//if span/1000000 > 1 {
		//	fmt.Printf("[time: %vms][size:%v][used:%v]random --> %s\n", span/1000000, pool.Size(), pool.Used(), random)
		//}
		fmt.Printf("[time: %vms][size:%v][used:%v]random --> %s\n", span/1000000, pool.Size(), pool.Used(), random)
	}
}

func TestPoolV4_Get(t *testing.T) {
	pool, err := NewPool("192.168.8.0/28")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("%+v\n", pool)
	start := ipAddress(uint32byte(pool.start))
	end := ipAddress(uint32byte(pool.end))
	fmt.Printf("start : %v\n", start)
	fmt.Printf("end   : %v\n", end)
	fmt.Println("------------------------------------------------------")
	var ip string
	if ip, err = pool.Get("192.168.8.1"); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("get ip success --> ", ip)
	}

	if ip, err = pool.Get("192.168.8.1"); err != nil {
		fmt.Println("get ip failed --> ", err)
	} else {
		fmt.Println("get ip success --> ", ip)
	}

	if err = pool.Put("192.168.8.1"); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("put ip success --> ", "192.168.8.1")
	}

	if ip, err = pool.Get("192.168.8.1"); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("get ip success --> ", ip)
	}
}
