package ipaddr

import (
	"encoding/binary"
	"math/rand"
	"time"
)

func uint32byte(i uint32) []byte {
	b := make([]byte, 4)
	binary.BigEndian.PutUint32(b, i)
	return b
}

func randInt32(min, max uint32, r *rand.Rand) uint32 {
	return uint32(r.Int31n(int32(max-min+1))) + min
}

func getRand() *rand.Rand {
	return rand.New(rand.NewSource(time.Now().UnixNano() + rand.Int63()))
}
