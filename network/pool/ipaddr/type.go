package ipaddr

import (
	"encoding/binary"
	"fmt"
	"net"
	"strings"
)

type ipAddress []byte

func (i *ipAddress) uint32() uint32 {
	if i == nil {
		return 0
	}
	return binary.BigEndian.Uint32(*i)
}

func (i *ipAddress) CIDR(mask int) string {
	return fmt.Sprintf("%s/%v", i.String(), mask)
}

func (i *ipAddress) String() string {
	if i == nil {
		return ""
	}
	var out []string
	for _, n := range *i {
		out = append(out, fmt.Sprintf("%d", n))
	}
	return strings.Join(out, ".")
}

func (i *ipAddress) IP() net.IP {
	if i == nil {
		return nil
	}
	return net.ParseIP(i.String())
}
