package network

import (
	"sync"
)

//
// RouteRecord
// @Description:
//
type RouteRecord struct {
	DeviceName string
	Network    string
	Deployed   bool
	Err        error
}

//
// Deploy
// @Description:
// @receiver r
//
func (r *RouteRecord) Deploy() {
	if r.Deployed {
		return
	}
	err := AddSystemRoute(r.Network, r.DeviceName)
	if err != nil {
		r.Deployed = false
		r.Err = err
		return
	} else {
		r.Deployed = true
	}
}

//
// SystemRouteTable
// @Description:
//
type SystemRouteTable struct {
	DeviceName string
	Records    map[string]*RouteRecord
	lock       sync.RWMutex
}

//
// NewSystemRouteTable
// @Description:
// @return *SystemRouteTable
//
func NewSystemRouteTable(devName string) *SystemRouteTable {
	return &SystemRouteTable{
		DeviceName: devName,
		Records:    make(map[string]*RouteRecord),
		lock:       sync.RWMutex{},
	}
}

//
// DeployAll
// @Description:
// @receiver t
//
func (t *SystemRouteTable) DeployAll() {
	t.lock.Lock()
	defer t.lock.Unlock()
	for n := range t.Records {
		t.Records[n].Deploy()
	}
}
