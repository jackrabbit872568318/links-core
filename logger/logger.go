package logger

import (
	log "github.com/cihub/seelog"
)

func Debug(v ...interface{}) {
	log.Debug(v...)
}

func Debugf(format string, params ...interface{}) {
	log.Debugf(format, params)
}

func Info(v ...interface{}) {
	log.Info(v...)
}

func Infof(format string, params ...interface{}) {
	log.Infof(format, params)
}

func Warn(v ...interface{}) error {
	return log.Warn(v...)
}

func Warnf(format string, params ...interface{}) error {
	return log.Warnf(format, params)
}

func Error(v ...interface{}) error {
	return log.Error(v...)
}

func Errorf(format string, params ...interface{}) error {
	return log.Errorf(format, params)
}
