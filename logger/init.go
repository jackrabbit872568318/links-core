package logger

import (
	"fmt"
	log "github.com/cihub/seelog"
	"os"
)

//
// getConfigString
// @Description:
// @param logPah
// @return string
//
func getConfigString(logPah string) string {
	return "" +
		"<seelog type=\"sync\">\n" +
		"    <outputs formatid=\"main\">\n" +
		"        <console />\n" +
		"        <rollingfile type=\"date\" datepattern=\"20060102.log\" filename=\"" + logPah + "links\" maxrolls=\"30\" fullname=\"true\"/>\n" +
		"    </outputs>\n" +
		"    <formats>\n" +
		"        <format id=\"main\" format=\"[%LEV][%Date(2006-01-02 15:04:05)][%File.%Line] %Msg%n\"/>\n" +
		"    </formats>\n" +
		"</seelog>"
}

func init() {
	storagePath := "./log/"
	s, err := os.Stat(storagePath)
	if os.IsNotExist(err) || !s.IsDir() {
		_ = os.MkdirAll(storagePath, 0644)
	}
	configString := getConfigString(storagePath)
	logger, err := log.LoggerFromConfigAsString(configString)
	if err != nil {
		fmt.Println("init log failed : ", err)
		return
	}
	err = log.ReplaceLogger(logger)
	if err != nil {
		fmt.Println("init log failed : ", err)
	}
}
