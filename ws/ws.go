package ws

import (
	"bytes"
	"errors"
	"github.com/gorilla/websocket"
	"net"
	"time"
)

//
// Wrap
// @Description:
// @param conn
// @return *Conn
//
func Wrap(conn *websocket.Conn) *Conn {
	return &Conn{
		conn:   conn,
		buf:    bytes.NewBuffer([]byte{}),
		closed: false,
	}
}

//
// Conn
// @Description:
//
type Conn struct {
	conn   *websocket.Conn
	buf    *bytes.Buffer
	closed bool
}

//
// readToBuf
// @Description:
// @receiver w
// @return n
// @return err
//
func (c *Conn) readToBuf() (n int, err error) {
	_, p, err := c.conn.ReadMessage()
	if err != nil {
		return 0, err
	}
	write, err := c.buf.Write(p)
	return write, err
}

//
// Read
// @Description:
// @receiver w
// @param b
// @return n
// @return err
//
func (c *Conn) Read(b []byte) (n int, err error) {
	if c.closed {
		return 0, errors.New("connection closed")
	}
	if len(b) == 0 {
		return
	}
	if c.buf.Len() < len(b) {
		defer func() {
			if err := recover(); err != nil {
				c.closed = true
			}
		}()
		for c.buf.Len() < len(b) {
			_, err = c.readToBuf()
			if err != nil {
				return 0, err
			}
		}
	}

	read, err := c.buf.Read(b)
	if err != nil {
		_ = c.Close()
		return 0, err
	}
	return read, nil
}

//
// Write
// @Description:
// @receiver w
// @param b
// @return n
// @return err
//
func (c *Conn) Write(b []byte) (n int, err error) {
	err = c.conn.WriteMessage(websocket.BinaryMessage, b)
	if err != nil {
		return 0, err
	}
	return len(b), nil
}

//
// Close
// @Description:
// @receiver w
// @return error
//
func (c *Conn) Close() error {
	c.closed = true
	return c.conn.Close()
}

//
// LocalAddr
// @Description:
// @receiver w
// @return net.Addr
//
func (c Conn) LocalAddr() net.Addr {
	return c.conn.LocalAddr()
}

//
// RemoteAddr
// @Description:
// @receiver w
// @return net.Addr
//
func (c Conn) RemoteAddr() net.Addr {
	return c.conn.RemoteAddr()
}

//
// SetDeadline
// @Description:
// @receiver w
// @param t
// @return err
//
func (c *Conn) SetDeadline(t time.Time) (err error) {
	err = c.conn.SetReadDeadline(t)
	if err != nil {
		return err
	}
	err = c.conn.SetWriteDeadline(t)
	if err != nil {
		return err
	}
	return nil
}

//
// SetReadDeadline
// @Description:
// @receiver w
// @param t
// @return error
//
func (c *Conn) SetReadDeadline(t time.Time) error {
	return c.conn.SetReadDeadline(t)
}

//
// SetWriteDeadline
// @Description:
// @receiver w
// @param t
// @return error
//
func (c *Conn) SetWriteDeadline(t time.Time) error {
	return c.conn.SetWriteDeadline(t)
}
